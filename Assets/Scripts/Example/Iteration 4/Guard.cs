﻿using System;
using UnityEngine;

namespace Example.Iteration_4
{
    [RequireComponent(typeof(Rigidbody))]
    public class Guard : MonoBehaviour
    {
        [SerializeField] private KeyCode jumpKey;
        [SerializeField] private float speedMovement;
        [SerializeField] private float rotationSpeed;
        [SerializeField] private float jumpForce;
        [SerializeField] private float raycastDistanceToFloor;

        private Rigidbody _rigidbody;

        private void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        private void Update()
        {
            CheckForJumpInputs();
            MoveByVelocity();
            RotateByVelocity();
        }

        private void CheckForJumpInputs()
        {
            if (!Input.GetKeyDown(jumpKey)) return;
            if (IsInFloor())
                Jump();
        }

        private void Jump()
        {
            //agregar linea de codigo para que el personaje salte
        }

        private bool IsInFloor()
        {
            return true; // si el usuario esta tocando el piso.
            return false; // en caso contrario
            //pista: todos los objetos del piso tienen un layer llamado "Floor"
        }

        private void RotateByVelocity()
        {
            if (IsInFloor())
                transform.rotation =
                    Quaternion.Euler(
                        transform.rotation.eulerAngles + new Vector3(0, GetAxisHorizontal() * rotationSpeed, 0));
        }

        private void MoveByVelocity()
        {
            if (IsInFloor()){}
                _rigidbody.velocity = transform.forward * GetAxisVertical() * speedMovement;
        }

        private float GetAxisVertical()
        {
            return Input.GetAxis("Vertical");
        }

        private float GetAxisHorizontal()
        {
            return Input.GetAxis("Horizontal");
        }
    }
}