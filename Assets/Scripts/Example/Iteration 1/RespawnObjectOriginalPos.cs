﻿using UnityEngine;

namespace Example.Iteration_1
{
    public class RespawnObjectOriginalPos : MonoBehaviour
    {
        private const float LIMIT_TIME = 2;
        private float _currentTime;
        private Rigidbody _rigidbody;
        private Vector3 _originPosition;
        private Quaternion _originRotation;

        void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _originPosition = transform.position;
            _originRotation = transform.rotation;
        }

        void Update()
        {
            if (_currentTime >= LIMIT_TIME)
                ResetObject();
            else
                _currentTime += Time.deltaTime;
        }

        private void ResetObject()
        {
            transform.position = _originPosition;
            transform.rotation = _originRotation;
            if (_rigidbody != null)
                _rigidbody.velocity = Vector3.zero;
            _currentTime = 0;
        }
    }
}