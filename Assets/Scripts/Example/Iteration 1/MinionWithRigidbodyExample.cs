﻿using UnityEngine;

namespace Example.Iteration_1
{
    public class MinionWithRigidbodyExample : MonoBehaviour
    {
        [SerializeField] private float _speed;
        [SerializeField] private float _rotationSpeed;
        private Rigidbody _rigidbody;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        void Update()
        {
            _rigidbody.velocity = transform.forward * GetAxisVertical();
            transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles + new Vector3(0, GetAxisHorizontal(), 0));
        }

        private float GetAxisVertical()
        {
            return Input.GetAxis("Vertical") * _speed;
        }

        private float GetAxisHorizontal()
        {
            return Input.GetAxis("Horizontal") * _rotationSpeed;
        }
    }
}