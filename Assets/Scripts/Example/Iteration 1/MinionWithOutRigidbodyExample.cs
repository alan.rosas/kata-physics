using UnityEngine;

namespace Example.Iteration_1
{
    public class MinionWithOutRigidbodyExample : MonoBehaviour
    {
        [SerializeField] private float _speed;

        void FixedUpdate()
        {
            var direction = transform.forward * GetAxisVertical();
            var pos =  transform.position + direction;
            Vector3 rotation = new Vector3(0, GetAxisHorizontal(), 0);
            var rot = transform.localRotation.eulerAngles + rotation;

            transform.SetPositionAndRotation(pos, Quaternion.Euler(rot));
        }

        private float GetAxisVertical()
        {
            return Input.GetAxis("Vertical") * _speed * Time.deltaTime;
        }

        private float GetAxisHorizontal()
        {
            return Input.GetAxis("Horizontal") * _speed * 10 * Time.deltaTime;
        }
    }
}