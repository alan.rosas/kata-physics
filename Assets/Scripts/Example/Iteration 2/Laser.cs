﻿using System;
using TMPro;
using UnityEngine;

namespace Example.Iteration_2
{
    public class Laser : MonoBehaviour
    {
        [SerializeField] [Range(0f, 10f)] private float _distance;
        [SerializeField] private TextMeshPro _textMeshPro;

        void Update()
        {
            var origin = transform.position;
            var direction = transform.right;
            Ray ray = new Ray(origin, direction * _distance);
            RaycastHit hit;
            Physics.Raycast(ray, out hit, _distance);
            Debug.DrawRay(origin, direction * _distance, Color.black);
            if (hit.transform != null)
            {
                Debug.Log(hit.transform.name);
                _textMeshPro.text = hit.transform.name;
            }
        }
    }
}