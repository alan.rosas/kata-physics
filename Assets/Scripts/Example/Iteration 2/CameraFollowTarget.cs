using System;
using UnityEngine;

namespace Example.Iteration_2
{
    public class CameraFollowTarget : MonoBehaviour
    {
        [SerializeField] private Transform _laserPoint;
        [SerializeField] private Transform _jetpackPoint;
        private Transform _currentPoint;

        private void Awake()
        {
            _currentPoint = _laserPoint;
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                _currentPoint = _laserPoint;
            }

            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                _currentPoint = _jetpackPoint;
            }

            transform.position = _currentPoint.position;
        }
    }
}