﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Example.Iteration_2
{
    public class JetPack : MonoBehaviour
    {
        private Rigidbody _rigidbody;
        private Vector3 _originPosition;
        [SerializeField] private float _force;
        [SerializeField] private List<ParticleSystem> _particleSystems;
        [SerializeField] private TextMesh _text;

        void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _originPosition = transform.position;
            StartCoroutine(ImpulseJetPack());
            PausedParticles();
        }

        private void Update()
        {
            _text.text = "Altura:" + ((int)(transform.position.y - _originPosition.y));
        }

        private IEnumerator ImpulseJetPack()
        {
            yield return new WaitForSeconds(2);
            PlayParticles();
            yield return new WaitForSeconds(1);
            Impulse();
        }


        void Impulse()
        {
            _rigidbody.AddForce(Vector3.up * _force, ForceMode.Impulse);
            StartCoroutine(Reset());
        }

        private IEnumerator Reset()
        {
            yield return new WaitForSeconds(5);
            PausedParticles();
            yield return new WaitForSeconds(10);
            _rigidbody.velocity = Vector3.zero;
            transform.position = _originPosition;
            StartCoroutine(ImpulseJetPack());
        }

        private void PlayParticles()
        {
            foreach (var particle in _particleSystems)
            {
                particle.Play();
            }
        }

        private void PausedParticles()
        {
            foreach (var particle in _particleSystems)
            {
                particle.Stop();
            }
        }
    }
}