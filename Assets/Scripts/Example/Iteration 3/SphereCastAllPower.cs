using System.Collections;
using UnityEngine;

namespace Example.Iteration_3
{
    public class SphereCastAllPower : Power
    {
        [Header("Casting")]
        [SerializeField] private float castRadius;
        [SerializeField] private float maxDistance;

        [Header("Explosion")]
        [SerializeField] private float explosionForce;
        [SerializeField] private float explosionRadius;
        
        [Header("Gizmos")]
        [SerializeField] private bool enableGizmo;
        [SerializeField] private float gizmoDistance;
        private float _originalGizmoDistance;
        
        protected override void OnAttack()
        {
            _originalGizmoDistance = gizmoDistance;
            CalculateGizmoDistance();
            var hits = Physics.SphereCastAll(transform.position, castRadius, transform.forward, maxDistance);
            if (hits.Length <= 0) return;
            
            Debug.Log("Golpeo con alguien");

            foreach (var hit in hits)
            {
                if (HasRigidbody(hit))
                {
                    Debug.Log("Si tiene un rigidbody, le agrego fuerza de explosion");

                    hit.transform.GetComponent<Rigidbody>().AddExplosionForce(explosionForce, hit.point, explosionRadius);
                }
            }
        }

        private void CalculateGizmoDistance()
        {
            InvokeRepeating(nameof(MoveGizmo),0,Time.deltaTime);
        }

        void MoveGizmo()
        {
            gizmoDistance += 1;
            if (gizmoDistance > maxDistance)
            {
                gizmoDistance = _originalGizmoDistance;
                CancelInvoke(nameof(MoveGizmo));
            }
        }

        private bool HasRigidbody(RaycastHit hit)
        {
            return hit.transform.GetComponent<Rigidbody>() != null;
        }

        void OnDrawGizmos()
        {
            if(!enableGizmo) return;

            Gizmos.color = Color.magenta;
            var resultPos = transform.position + transform.forward * gizmoDistance;
            Gizmos.DrawSphere(resultPos, castRadius);
        }
    }
}