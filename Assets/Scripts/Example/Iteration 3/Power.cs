﻿using UnityEngine;
using UnityEngine.Events;

namespace Example.Iteration_3
{
    public abstract class Power : MonoBehaviour
    {
        private readonly UnityEvent _onAttack = new UnityEvent();

        private void Awake()
        {
            _onAttack.AddListener(OnAttack);
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                _onAttack.Invoke();
            }
        }

        protected abstract void OnAttack();
    }
}