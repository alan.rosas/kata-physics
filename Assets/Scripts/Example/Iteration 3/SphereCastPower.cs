using UnityEngine;

namespace Example.Iteration_3
{
    public class SphereCastPower : Power
    {
        [Header("Casting")]
        [SerializeField] private float castRadius;
        
        [Header("Explosion")]
        [SerializeField] private float explosionForce;
        [SerializeField] private float explosionRadio;
        
        [Header("Gizmos")]
        [SerializeField] private bool enableGizmo;
        [SerializeField] private float gizmoDistance;
        [SerializeField] private float maxDistance;

        private float _originalGizmoDistance;

        protected override void OnAttack()
        {
            _originalGizmoDistance = gizmoDistance;
            CalculateGizmoDistance();
            RaycastHit hit;
            if (Physics.SphereCast(transform.position, castRadius, transform.forward, out hit))
            {
                Debug.Log("Golpeo con " + hit.transform.name);
                
                if (HasRigidbody(hit))
                {
                    Debug.Log("Si tiene un rigidbody, le agrego fuerza de explosion");

                    hit.transform.GetComponent<Rigidbody>().AddExplosionForce(explosionForce, hit.point, explosionRadio);
                }
            }
        }
        
        private void CalculateGizmoDistance()
        {
            InvokeRepeating(nameof(MoveGizmo),0,Time.deltaTime);
        }

        void MoveGizmo()
        {
            gizmoDistance += 1;
            if (gizmoDistance > maxDistance)
            {
                gizmoDistance = _originalGizmoDistance;
                CancelInvoke(nameof(MoveGizmo));
            }
        }

        private bool HasRigidbody(RaycastHit hit)
        {
            return hit.transform.GetComponent<Rigidbody>() != null;
        }
        
        void OnDrawGizmos()
        {
            if(!enableGizmo) return;

            Gizmos.color = Color.magenta;
            var resultPos = transform.position + transform.forward * gizmoDistance;
            Gizmos.DrawSphere(resultPos, castRadius);
        }
    }
}