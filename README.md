# Kata-Physics

#Iteración 1 - Movimiento
Explicación, Rigidbody
Collider/Trigger

	Ejemplo: crear escena con cubos cayendo, usar layer/tags para evitar colisiones

Actividad: Una Catapulta,Poner un objeto “bala” y otro “peso” que cae

#Iteración 2 - Golpes

    Ejemplo: láser que dice el nombre de lo que está colisionando. JetPack para mostrar el addforce y sus modos

Actividad: Que los personajes al golpear se desplacen para el lado contrario

#Iteración 3 - Poderes

    Ejemplo: Un personaje que camina, y cuando apretas un boton, empuja hacia su adelante todo lo que tiene cerca. 
        - Physics.OverlapXXX

Actividad: Con lo ya visto crear 2 poderes para sus personajes. Usar Vector3 en rotaciones, escalas, movimiento. 

#Iteración 4 - Ambiente

    Ejemplo: Ya tuviste demasiados
    
Armar un personaje que se mueva y salte con Rigidbody.
